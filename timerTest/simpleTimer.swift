//
//  simpleTimer.swift
//  timerTest
//
//  Created by Niall Cooling on 06/10/2014.
//  Copyright (c) 2014 Feabhas Ltd. All rights reserved.
//
import UIKit
import Foundation


public protocol timerObservor {
    func update(currentValue:Int)
    func completed()
}

public class simpleTimer : NSObject {
    
    var timer = NSTimer()
    var counter = 0
    var targetCount = 10
    var msCounter:Float32 = 0.0
    var observer:timerObservor?
    //==============================================================================
    public init(counterInitalValue:Int, callbackObject: timerObservor) {
        super.init()
        targetCount = counterInitalValue
        self.observer = callbackObject
        resetCounters(targetCount)
    }
    
    //==============================================================================
    func cleanup() {
        if timer.valid  {
            timer.invalidate()
        }
    }
    //==============================================================================
    func notifyFinished() {
        observer?.completed()
    }
    //==============================================================================
    func notifyTick() {
        observer?.update(counter)
    }
    //==============================================================================
    func processTick() {
        msCounter -= 0.1
        if msCounter > 0.0 {
            counter = Int(msCounter)+1
            self.notifyTick()
        }
        else {
            stop()
            notifyFinished()
        }
    }
    //==============================================================================
    public func start() {
        if !timer.valid {

            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("processTick"), userInfo: nil, repeats: true)
        }
    }
    //==============================================================================
    func pause() {
        if timer.valid  {
            timer.invalidate()
        }
    }
    //==============================================================================
    func stop() {
        if timer.valid  {
            timer.invalidate()
        }
        resetCounters(0)
    }

    
    //==============================================================================
    func reset() {
        if timer.valid  {
            timer.invalidate()
        }
        resetCounters(targetCount)
    }
    
    //==============================================================================
    func resetCounters(initalValue:Int) {
        msCounter = Float32(initalValue)
        counter = Int(msCounter)
    }
    
}








