//
//  ViewController.swift
//  timerTest
//
//  Created by Niall Cooling on 06/10/2014.
//  Copyright (c) 2014 Feabhas Ltd. All rights reserved.
//

import UIKit
import Foundation



class ViewController: UIViewController, UITextFieldDelegate, timerObservor ,UIPickerViewDelegate , UIPickerViewDataSource {
    
    var timer1: simpleTimer!

    @IBOutlet var lableCounter: UILabel!
    @IBOutlet var editBarButton: UIBarButtonItem!
    
    @IBOutlet var timeInSeconds: UIPickerView!
    @IBOutlet var secsLabel: UILabel!
    @IBOutlet var minsLabel: UILabel!
    
    var minsArray : NSMutableArray  = NSMutableArray()
    var secsArray : NSMutableArray  = NSMutableArray()
    
    enum States {
        case Uninitialised, Ready, SettingTimerValue, Running, Paused, Finishing, Finshed
    }
    
    enum Events {
        case Init, Edit, Done, Start, Pause, Reload, Tick, Completed
    }
    
    var currentState = States.Uninitialised
    var theCurrentTimervalue : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //timeInSeconds.add
        // Do any additional setup after loading the view, typically from a nib.
        
        timer1 = simpleTimer(counterInitalValue:10, callbackObject: self)
        
        timeInSeconds.delegate = self
        timeInSeconds.dataSource = self
        timer1.observer = self

        // setup arrays for timer Picker
        for i in 0...60
        {
            //create arrays with 0-59 mins
            minsArray.addObject(String(i))
        }
        for var i = 0; i < 60; i += 5
        {
            //create arrays with 0-55  secs in 5 sec intervals
            secsArray.addObject(String(i));
        }
        
        stateMachine(Events.Init)
    }
    
    @IBAction func clearCounterPressed(sender: AnyObject) {
        stateMachine(Events.Reload)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pauseCounterPressed(sender: AnyObject) {
        stateMachine(Events.Pause)
    }

    @IBAction func startCounterPressed(sender: AnyObject) {
        stateMachine(Events.Start)
    }
    
    // callback function from timer
    func update(currentValue:Int) {
        theCurrentTimervalue = currentValue
        stateMachine(Events.Tick)
    }
    
    func completed() {
        theCurrentTimervalue = 0
        stateMachine(Events.Completed)
    }
    
    
    //////// PICKER VIEW FUNCTIONS /////////////
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return minsArray.count
        }
        else {
            return secsArray.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        let pickerData = [ minsArray, secsArray ]
        return String(pickerData[component][row] as NSString)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateLabel()
    }
    
    //MARK -Instance Methods
    func updateLabel(){
        let pickerData = [ minsArray, secsArray ]
        let mins = String(pickerData[0][timeInSeconds.selectedRowInComponent(0)] as NSString).toInt()
        let secs = String(pickerData[1][timeInSeconds.selectedRowInComponent(1)] as NSString).toInt()
        let totalTime = (mins!*60)+secs!
        timer1.targetCount = totalTime
        timer1.reset()
        lableCounter.textColor = UIColor.blackColor()
        lableCounter.text = String(timer1.counter)
    }

    @IBAction func editButtonPressed(sender: UIBarButtonItem) {
        stateMachine(Events.Edit)
    }
    
    func hidePicker() {
        timeInSeconds.hidden = true
        minsLabel.hidden = true
        secsLabel.hidden = true
    }
    
    func showPicker() {
        timeInSeconds.hidden = false
        minsLabel.hidden = false
        secsLabel.hidden = false
    }
    /////// STATE MACHINE //////////////////////////
    func stateMachine(e:Events) {
        switch(e) {
        case .Init:
            lableCounter.text = String(timer1.counter)
            hidePicker()
            currentState = States.Ready
            println("Ready")
        case .Edit:
            if currentState == States.Ready {
                showPicker()
                editBarButton.title = "Done"
                currentState = States.SettingTimerValue
                println("SettingTimerValue")
            }
            else if currentState == States.SettingTimerValue {
                hidePicker()
                editBarButton.title = "Edit"
                currentState = States.Ready
                println("Ready")
            }
        case .Start:
            if currentState == States.Ready  {
                timer1.start()
                currentState = States.Running
                println("Running")
            }
            else if (currentState == States.Paused) {
                timer1.start()
                currentState = States.Running
                println("Running")
            }
        case .Tick:
            lableCounter.text = String(theCurrentTimervalue)
            if currentState == States.Running {
                if theCurrentTimervalue < 4 {
                    lableCounter.textColor = UIColor.redColor()
                    currentState = States.Finishing
                    println("Finishing")
                }
            }
        case .Completed:
            lableCounter.text = String(theCurrentTimervalue)
            if currentState == States.Finishing {
                if theCurrentTimervalue == 0 {
                    currentState = States.Finshed
                    println("Finished")
                }
            }
        case .Reload:
            if currentState == States.Finshed || currentState == States.Paused  {
                timer1.reset()
                lableCounter.textColor = UIColor.blackColor()
                lableCounter.text = String(timer1.counter)
                currentState = States.Ready
                println("Ready")
            }
        case .Pause:
            if currentState == States.Running  {
                timer1.pause()
                currentState = States.Paused
                println("Paused")
            }
        default:
            lableCounter.text = String(timer1.counter)
            timeInSeconds.hidden = true
            minsLabel.hidden = true
            secsLabel.hidden = true
            println("Default")
        }
    }
}

