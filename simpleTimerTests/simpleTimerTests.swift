//
//  simpleTimerTests.swift
//  simpleTimerTests
//
//  Created by Niall Cooling on 09/11/2014.
//  Copyright (c) 2014 Feabhas Ltd. All rights reserved.
//

import UIKit
import XCTest
import timerTest

class simpleTimerTests: XCTestCase, timerObservor {
    
    var expectCallback : XCTestExpectation!
    var t : simpleTimer!
    var count : Int = 0
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        count = 0
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testSimpleTimer() {
        expectCallback = expectationWithDescription("simple timer calls")
        t = simpleTimer(counterInitalValue:2, callbackObject: self)
        t.start()
        self.waitForExpectationsWithTimeout(5.0, handler: nil)
        XCTAssertEqual(count/10, 2, "Callback not called correct number of time") // /10 as callback in 100ms intervals
    }
    
    
    ///// timerObservor callbacks
    
    func update(currentValue: Int) {
        count++
    }
    
    func completed() {
        count++
        expectCallback.fulfill()
    }
}
